# Laravel select box using select2 (search,placeholder,clear selected box data)

Laravel select box using select2 (search,placeholder,clear selected box data)

Project youtube tutorial link 
======

>>>
Video Tutorial for this project can be found on  https://www.youtube.com/watch?v=-OpcPKI7_yE

Usage
======
    
 in **view**
 
 ```script
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
 ```
  ```html
  <select  id="nameid">
  
        <option></option>
        
        <option>tino</option>
        <option>xyh hr</option>
        <option>abc</option>
        <option>gdryvfg</option>
        <option>xyh hr</option>
        <option>abacas</option>

    </select>
  ```
```script
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

```


```script
<script type="text/javascript">

      $("#nameid").select2({
            placeholder: "Select a Name",
            allowClear: true
        )};
 </script>
```

- For **placeholder** use empty <option></option>
- For clear the select box selected data use **allowClear**



Important directory in this project
======
- laraveltoastr(projectname)
    - app
        - Http
            - controllers
                 - TestController.php (controller)
    - routes
         -web.php   
    - resources
        - views
             -testview.blade.php  (view)
    

For more about select2
======
- https://select2.github.io/
- https://select2.github.io/examples.html
